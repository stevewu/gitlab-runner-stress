module gitlab.com/gitlab-runner-stress/gitlab-runner-stress

go 1.15

require (
	github.com/bombsimon/logrusr v1.0.0
	github.com/go-logr/logr v0.3.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
)
