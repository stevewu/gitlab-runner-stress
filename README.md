# GitLab Runner Stress

## Goal

- Have the ability to create and run multiple pipelines simultaneously to test the
performance and autoscaling functionality for the runner the picks up the jobs.
- Able to run multiple types of pipelines such as C, Go, Ruby, and Node application.
- Run benchmarks on the infrastructure to get CPU, Memory, Network, and I/O.

## Usage

⚠️ 🗣 This shouldn't be used on GitLab.com, since it can be considered abusive behavior and is susceptible to limits ⚠️ 🗣

### Trigger pipeline

```shell
export GITLAB_PAT=token
go run cmd/gitlab-runner-stress/gitlab-runner-stress.go trigger --gitlab-instance https://gitlab.com --count 2
```

### Trigger benchmarks

```shell
export GITLAB_PAT=token
go run cmd/gitlab-runner-stress/gitlab-runner-stress.go benchmark --gitlab-instance https://gitlab.com
```
