export CGO_ENABLED = 0

out_dir:
	@mkdir -p out

.PHONY: test
test: out_dir
	go test -v -coverprofile=out/cover.out ./...
	go tool cover -func out/cover.out

.PHONY: test-race
test-race: CGO_ENABLED=1
test-race:
	go test -v -race ./...

.PHONY: build
build: out_dir
	go build -o out/bin/gitlab-runner-stress cmd/gitlab-runner-stress/gitlab-runner-stress.go

.PHONY: lint
lint: OUT_FORMAT ?= line-number
lint: SHELL=/bin/bash
lint: out_dir
	set -o pipefail; golangci-lint run --out-format $(OUT_FORMAT) | tee out/gl-code-quality-report.json

