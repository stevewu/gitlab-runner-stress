package benchmark

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab"
)

const triggerProjectPath = "gitlab-org/ci-cd/gitlab-runner-stress/trigger"

// runBenchmarksVarKey is the key for the variable that is used to trigger the
// benchmark pipeline only
// https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress/benchmarks
const runBenchmarksVarKey = "RUN_BENCHMARKS"

type benchmarkAction struct {
	logger logr.Logger
}

// NewCommand returns a pointer for the cli command that is the point of entry
// of the package.
func NewCommand(logger logr.Logger) *cli.Command {
	benchmark := benchmarkAction{
		logger: logger,
	}

	return &cli.Command{
		Name: "benchmark",
		Usage: "Create a new pipeline for the trigger project " +
			"(http://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress/trigger) " +
			"with just benchmarks running (https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress/benchmarks)",
		Action: benchmark.action,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "gitlab-instance",
				Usage:    `The base URL of the GitLab instance, for example "https://gitlab.com"`,
				Required: true,
			},
			&cli.StringFlag{
				Name: "trigger-project-path",
				Usage: "Path for the trigger project (http://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress/trigger). " +
					"You only have to specify this when the trigger project is in a different path",
				Value: triggerProjectPath,
			},
		},
	}
}

func (t *benchmarkAction) action(cli *cli.Context) error {
	client := gitlab.NewClient(t.logger, cli.String("gitlab-pat"), cli.String("gitlab-instance"))

	projectID, err := client.ProjectID(cli.Context, cli.String("trigger-project-path"))
	if err != nil {
		return fmt.Errorf("getting project id for %q: %v", cli.String("trigger-project-path"), err)
	}

	variables := []gitlab.JobVariable{
		{
			Key:   runBenchmarksVarKey,
			Type:  gitlab.EnvironmentVariableType,
			Value: "true",
		},
	}

	url, err := client.CreatePipeline(cli.Context, projectID, variables)
	if err != nil {
		return fmt.Errorf("creating pipeline for project %d: %v", projectID, err)
	}

	t.logger.Info("Created benchmark pipeline", "url", url)

	return nil
}
