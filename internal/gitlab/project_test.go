package gitlab

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bombsimon/logrusr"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab/testdata"
)

func TestClient_ProjectID(t *testing.T) {
	tests := map[string]struct {
		expectedID         int
		responseStatusCode int
		response           string
		slowResponse       bool
		expectedErr        bool
	}{
		"200 OK": {
			expectedID:         123,
			responseStatusCode: http.StatusOK,
			response:           testdata.ProjectQuerySuccessResp,
		},
		"500 Internal Server Error": {
			responseStatusCode: http.StatusInternalServerError,
			expectedErr:        true,
		},
		"project id is not an int": {
			responseStatusCode: http.StatusOK,
			response:           testdata.ProjectQueryInvalidProjectResp,
			expectedErr:        true,
		},
		"invalid response": {
			responseStatusCode: http.StatusOK,
			response:           "junk",
			expectedErr:        true,
		},
		"timeout": {
			responseStatusCode: http.StatusOK,
			slowResponse:       true,
			expectedErr:        true,
		},
	}

	for testName, tt := range tests {
		t.Run(testName, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				require.Equal(t, fmt.Sprintf("Bearer: %s", testToken), r.Header.Get("Authorization"))
				require.Equal(t, graphQLEndpoint, r.URL.String())

				if tt.slowResponse {
					time.Sleep(100 * time.Millisecond)
				}

				w.WriteHeader(tt.responseStatusCode)
				fmt.Fprint(w, tt.response)
			}))
			defer ts.Close()

			client := NewClient(logrusr.NewLogger(logrus.New()), testToken, ts.URL)
			client.restAPI = &http.Client{Timeout: 50 * time.Millisecond}

			url, err := client.ProjectID(context.Background(), "gitlab-runner-stress/trigger")
			require.Equal(t, tt.expectedErr, err != nil)
			require.Equal(t, tt.expectedID, url)
		})
	}
}
