package gitlab

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-logr/logr"
	"github.com/machinebox/graphql"
)

const graphQLEndpoint = "/api/graphql"
const restAPIEndpoint = "/api/v4"

// Client interacts with the GitLab API.
type Client struct {
	graphQL      *graphql.Client
	restAPI      *http.Client
	restEndpoint string
	pat          string
	logger       logr.Logger
}

// NewClient creates a new Client with the specified  PAT token. To learn more
// about the PAT token checkout
// https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
func NewClient(logger logr.Logger, gitlabPAT string, gitlabInstance string) *Client {
	restAPI := http.Client{
		Timeout: 5 * time.Second,
	}

	return &Client{
		graphQL:      graphql.NewClient(fmt.Sprintf("%s%s", gitlabInstance, graphQLEndpoint)),
		restAPI:      &restAPI,
		restEndpoint: fmt.Sprintf("%s%s", gitlabInstance, restAPIEndpoint),
		pat:          gitlabPAT,
		logger:       logger,
	}
}

// graphQLAuthHeader gets the key and value of the HTTP header needed to authenticate
// with the graphQL API.
// https://docs.gitlab.com/ee/api/graphql/getting_started.html#authorization
func (c *Client) graphQLAuthHeader() (string, string) {
	return "Authorization", fmt.Sprintf("Bearer: %s", c.pat)
}

// restAPIAuthHeader gets the key and value for the HTTP header needed to
// authenticate with the rest API.
// https://docs.gitlab.com/ee/api/README.html#personalproject-access-tokens
func (c *Client) restAPIAuthHeader() (string, string) {
	return "PRIVATE-TOKEN", c.pat
}
