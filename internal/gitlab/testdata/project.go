package testdata

// ProjectQuerySuccessResp project response with a valid GID.
const ProjectQuerySuccessResp = `
{
  "data": {
    "project": {
      "id": "gid://gitlab/Project/123"
    }
  }
}
`

// ProjectQuerySuccessResp project response with a invalid GID, where the id is
// set to a string.
const ProjectQueryInvalidProjectResp = `
{
  "data": {
    "project": {
      "id": "gid://gitlab/Project/Invalid"
    }
  }
}
`
